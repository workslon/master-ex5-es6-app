import React from 'react';
import { render } from 'react-dom';
import AppActions from '../actions/AppActions';
import StatusConstants from '../constants/StatusConstants';
import BookModel from '../models/Book';
import { IndexLink } from 'react-router';

export default class CreateBook extends React.Component {
  static propTypes = {
    notifications: React.PropTypes.object
  };

  constructor(props) {
    super(props);
    this._createBook = this._createBook.bind(this);
    this._validate = this._validate.bind(this);
  }

  componentDidUpdate() {
    let notifications = this.props.notifications || {};
    let status = notifications.status;

    if (status === StatusConstants.SUCCESS) {
      let refs = this.refs;
      refs.isbn.value = refs.title.value = refs.year.value = '';
    }
  }

  _createBook(e) {
    e.preventDefault();

    let refs = this.refs || {};
    let isbn = refs.isbn || {};
    let title = refs.title || {};
    let year = refs.year || {};

    AppActions.createBook(BookModel, {
      isbn: isbn.value,
      title: title.value,
      year: parseInt(year.value)
    });
  }

  _validate(e) {
    AppActions.validate(BookModel, e.target.id, e.target.value);
  }

  render() {
    let notifications = this.props.notifications || {};
    let errors = notifications.errors || {};
    let status = notifications.status;

    return (
      <form>
        <h3>Create Book</h3>
        <div className="form-group">
          <label forHtml="isbn">ISBN</label>
          <input defaultValue="" onInput={this._validate} ref="isbn" type="text" className="form-control" id="isbn" placeholder="ISBN" />
          {errors.isbn && <span className="text-danger">{errors.isbn}</span>}
        </div>
        <div className="form-group">
          <label forHtml="title">Title</label>
          <input defaultValue="" onInput={this._validate} ref="title" type="text" className="form-control" id="title" placeholder="Title" />
          {errors.title && <span className="text-danger">{errors.title}</span>}
        </div>
        <div className="form-group">
          <label forHtml="year">Year</label>
          <input defaultValue="" onInput={this._validate} ref="year" type="text" className="form-control" id="year" placeholder="Year" />
          {errors.year && <span className="text-danger">{errors.year}</span>}
        </div>
        <button type="submit" onClick={this._createBook} className="btn btn-default">Submit</button>
        {status === StatusConstants.SUCCESS && <p className="bg-success">Success!</p>}
        {status === StatusConstants.PENDING && <p className="bg-info">Creating...</p>}
        <IndexLink className="back" to="/">&laquo; back</IndexLink>
      </form>
    );
  }
};