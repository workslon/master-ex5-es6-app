import React from 'react';
import AppActions from '../actions/AppActions';
import AppStore from '../stores/AppStore';
import StatusConstants from '../constants/StatusConstants';
import BookModel from '../models/Book';
import { IndexLink } from 'react-router';

export default class UpdateBook extends React.Component {
  static propTypes = {
    params: React.PropTypes.object,
    notifications: React.PropTypes.object
  };

  constructor(props) {
    super(props);
    this._updateBook = this._updateBook.bind(this);
  }

  componentWillMount() {
    this.book = AppStore.getBook(this.props.params.id);
  }

  _updateBook(e) {
    e.preventDefault();

    let refs = this.refs || {};
    let isbn = refs.isbn || {};
    let title = refs.title || {};
    let year = refs.year || {};

    AppActions.updateBook(BookModel, this.book, {
      title: title.value,
      year: parseInt(year.value)
    });
  }

  _validate(e) {
    AppActions.validate(BookModel, e.target.id, e.target.value);
  }

  render() {
    let notifications = this.props.notifications || {};
    let errors = notifications.errors || {};
    let status = notifications.status;

    return (
      <div>
        <h3>Update Book</h3>
        {this.book ?
          <form>
            <div className="form-group">
              <label forHtml="isbn">ISBN</label>
              <input defaultValue={this.book.isbn} disabled="disabled" ref="isbn" type="text" className="form-control" id="isbn" placeholder="ISBN" />
            </div>
            <div className="form-group">
              <label forHtml="title">Title</label>
              <input defaultValue={this.book.title} onInput={this._validate} ref="title" type="text" className="form-control" id="title" placeholder="Title" />
              {errors.title && <span className="text-danger">{errors.title}</span>}
            </div>
            <div className="form-group">
              <label forHtml="year">Year</label>
              <input defaultValue={this.book.year} onInput={this._validate} ref="year" type="text" className="form-control" id="year" placeholder="Year" />
              {errors.year && <span className="text-danger">{errors.year}</span>}
            </div>
            <button type="submit" onClick={this._updateBook} className="btn btn-default">Submit</button>
            {status === StatusConstants.SUCCESS && <p className="bg-success">Success!</p>}
            {status === StatusConstants.PENDING && <p className="bg-info">Updating...</p>}
            <IndexLink className="back" to="/">&laquo; back</IndexLink>
          </form>
        : <div>No book found...</div>}
      </div>
    );
  }
};