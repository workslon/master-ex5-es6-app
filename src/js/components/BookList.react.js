import React from 'react';
import StatusConstants from '../constants/StatusConstants';
import { Link } from 'react-router';
import Book from './Book.react';

const BookList = ({
  books,
  notifications
}) => (
  <div>
    <Link className="create-book btn btn-success bt-sm" to="/create">+ Add Book</Link>
    {status === StatusConstants.PENDING && <p className="bg-info">Deleting...</p>}
    {books.length ?
      <table className="table table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th>ISBN</th>
            <th>Title</th>
            <th>Year</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {books.map((book, i) => {
            return (
              <Book key={i} nr={i + 1} book={book} />
            );
          })}
        </tbody>
      </table>
      : <div>Loading books...</div>}
  </div>
);

export default BookList;