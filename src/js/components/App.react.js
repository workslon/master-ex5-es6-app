import React from 'react';
import { IndexLink } from 'react-router';
import AppActions from '../actions/AppActions';
import AppStore from '../stores/AppStore';
import StatusConstants from '../constants/StatusConstants';
import ReactRotuer from 'react-router';
import BookModel from '../models/Book';

export default class App extends React.Component {
  constructor() {
    super();
    this._onChange = this._onChange.bind(this);
    this._clearNotifications = this._clearNotifications.bind(this);
  }

  state = {
    books: AppStore.getAllBooks(),
    notifications: AppStore.getNotifications()
  }

  static contextTypes = {
    router: React.PropTypes.object.isRequired
  };

  static childContextTypes = {
    router: React.PropTypes.object.isRequired
  };

  componentDidMount() {
    AppActions.getBooks(BookModel);
    this.context.router.listenBefore(this._clearNotifications);
    AppStore.addChangeListener(this._onChange);
  }

  componentWillUnmount() {
    AppStore.removeChangeListener(this._onChange);
  }

  _clearNotifications() {
    let notifications = this.state.notifications;
    let isStatus = notifications.status !== StatusConstants.IDLE;
    let isErrors = Object.keys(notifications.errors).length;

    if (isStatus || isErrors) {
      AppActions.clearNotifications();
    }
  }

  _onChange() {
    this.setState({
      books: AppStore.getAllBooks(),
      notifications: AppStore.getNotifications()
    });
  }

  _renderChildren() {
    return React.Children.map(this.props.children, (child => {
      return React.cloneElement(child, {
        books: this.state.books,
        notifications: this.state.notifications
      });
    }).bind(this));
  }

  render() {
    return (
      <div>
        <header className="well container">
          <h4>Example 5 - Re-encoding React.js Application in ES6 in 5 Steps</h4>
          <a target="_block" href="https://bitbucket.org/workslon/master-thesis/wiki/chapter5.md">Related Thesis Chapter</a>
          &nbsp;/&nbsp;
          <a target="_block" href="https://bitbucket.org/workslon/master-ex5-es6-app/src">Source Code</a>
        </header>
        <div className="page-header">
          <h1><IndexLink to="/">Public Library</IndexLink></h1>
        </div>
        { this._renderChildren() }
      </div>
    );
  }
}