import React from 'react';
import { Link } from 'react-router';
import AppActions from '../actions/AppActions';
import BookModel from '../models/Book';

export default class Book extends React.Component {
  constructor(props) {
    super(props);
    this._deleteBook = this._deleteBook.bind(this);
  }

  _deleteBook() {
    AppActions.deleteBook(BookModel, this.props.book);
  }

  render() {
    let book = this.props.book,
        updatePath = `/update/${book.objectId}`;

    return (
      <tr>
        <td scope="row">{this.props.nr}</td>
        <td>{book.isbn}</td>
        <td>{book.title}</td>
        <td>{book.year}</td>
        <td className="action-links">
          <Link className="btn btn-primary btn-xs" to={updatePath}>Update</Link>
          <a className="btn btn-danger btn-xs" onClick={this._deleteBook}>Delete</a>
        </td>
      </tr>
    );
  }
};