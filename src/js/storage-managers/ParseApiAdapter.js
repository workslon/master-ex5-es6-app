import sTORAGEmANAGER from 'sTORAGEmANAGER';

const ParseAdapter = new sTORAGEmANAGER({
  name: 'parse-rest-api',
  appId: 'MyH7zW1',
  apiUrl: 'https://public-library-api.herokuapp.com/api/classes'
});

export default ParseAdapter;