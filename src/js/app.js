import React from 'react';
import { render } from 'react-dom';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';
import App from './components/App.react';
import BookList from './components/BookList.react';
import CreateBook from './components/CreateBook.react';
import UpdateBook from './components/UpdateBook.react';

render((
  <Router history={hashHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={BookList} />
      <Route path="/create" component={CreateBook} />
      <Route path="/update/:id" component={UpdateBook} />
    </Route>
  </Router>
), document.getElementById('app'));