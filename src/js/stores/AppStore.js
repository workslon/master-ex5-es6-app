import AppDispatcher from '../dispatchers/AppDispatcher';
import ActionConstants from '../constants/ActionConstants';
import StatusConstants from '../constants/StatusConstants';
import { EventEmitter } from 'events';
import { eNUMERATION } from 'eNUMERATION';

const CHANGE_EVENT = 'change';

let books = [];
let notifications = {
  status: StatusConstants.IDLE,
  errors: {}
};

const AppStore = Object.assign({}, EventEmitter.prototype, {
  getBook(id) {
    return books.filter(book => {
      return book.objectId === id;
    })[0];
  },

  getAllBooks() {
    return books;
  },

  getNotifications() {
    return notifications;
  },

  emitChange() {
    this.emit(CHANGE_EVENT);
  },

  addChangeListener(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  removeChangeListener(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }
});

AppDispatcher.register(action => {
  switch(action.type) {
    // -- Get all books
    case ActionConstants.REQUEST_BOOKS_SUCCESS:
      try {

        books = action.result;
        AppStore.emitChange();
      } catch (e) {
        alert('Unvalid remote response format!');
      }
      break;

    // -- Create book Pending
    case ActionConstants.REQUEST_BOOK_SAVE:
      notifications.status = StatusConstants.PENDING;
      AppStore.emitChange();
      break;

    // -- Create book Success
    case ActionConstants.BOOK_SAVE_SUCCESS:
      try {
        action.data.objectId = JSON.parse(action.result).objectId;
        books.push(action.data);
        notifications.errors = {};
        notifications.status = StatusConstants.SUCCESS;
        AppStore.emitChange();
      } catch (e) {}
      break;

    // -- Create book Error
    case ActionConstants.BOOK_SAVE_ERROR:
      notifications.status = StatusConstants.ERROR;
      notifications.errors = action.error;
      AppStore.emitChange();
      break;

    // -- Update book Pending
    case ActionConstants.REQUEST_BOOK_UPDATE:
      notifications.status = StatusConstants.PENDING;
      AppStore.emitChange();
      break;

    // --- Update book Success
    case ActionConstants.BOOK_UPDATE_SUCCESS:
      books = books.map(book => {
        if (book.objectId === action.data.objectId) {
          book.title = action.data.title;
          book.year = action.data.year;
        }
        return book;
      });
      notifications.errors = {};
      notifications.status = StatusConstants.SUCCESS;
      AppStore.emitChange();
      break;

    // -- Update book Error
    case ActionConstants.BOOK_UPDATE_ERROR:
      notifications.status = StatusConstants.ERROR;
      notifications.errors = action.error;
      AppStore.emitChange();
      break;

    // -- Destroy book Pending
    case ActionConstants.REQUEST_BOOK_DESTROY:
      notifications.status = StatusConstants.PENDING;
      AppStore.emitChange();
      break;

    // -- Destroy book Success
    case ActionConstants.BOOK_DESTROY_SUCCESS:
      books = books.filter(book => {
        return book.objectId !== action.data.objectId;
      });
      notifications.status = StatusConstants.IDLE;
      AppStore.emitChange();
      break;

    // --- Client Validation error
    case ActionConstants.BOOK_VALIDATION_ERROR:
      Object.keys(action.errors).map(key => {
        notifications.errors[key] = action.errors[key];
      });

      notifications.status = StatusConstants.ERROR;
      AppStore.emitChange();
      break;

    // --- Non-unique ISBN
    case ActionConstants.NON_UNIQUE_ISBN:
      notifications.status = StatusConstants.ERROR;
      notifications.errors = {
        isbn: 'The book with such ISBN already exists!'
      };
      AppStore.emitChange();
      break;

    // --- Clear all notifications (eather errors or success)
    case ActionConstants.CLEAR_NOTIFICATIONS:
      notifications = {
        status: StatusConstants.IDLE,
        errors: {}
      };
      AppStore.emitChange();
      break;
  }
});

export default AppStore;