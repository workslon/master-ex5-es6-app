import AppDispatcher from '../dispatchers/AppDispatcher';
import ActionConstants from '../constants/ActionConstants';
import adapter from '../storage-managers/ParseApiAdapter';

export default {
  validate(modelClass, key, value) {
    let errors = {};
    errors[key] = modelClass.check(key, value).message;

    AppDispatcher.dispatch({
      type: ActionConstants.BOOK_VALIDATION_ERROR,
      errors: errors
    });
  },

  getBooks(modelClass) {
    let promise = adapter.retrieveAll(modelClass);

    AppDispatcher.dispatchAsync(promise, {
      request: ActionConstants.REQUEST_BOOKS,
      success: ActionConstants.REQUEST_BOOKS_SUCCESS,
      failure: ActionConstants.REQUEST_BOOKS_ERROR
    });
  },

  createBook(modelClass, data) {
    adapter
      .retrieve(modelClass, '', data.isbn)
      .then(records => {
        try {
          if (records.length) {
            AppDispatcher.dispatch({
              type: ActionConstants.NON_UNIQUE_ISBN
            });
          } else {
            data.runCloudCode = true;
            AppDispatcher.dispatchAsync(adapter.add(modelClass, data), {
              request: ActionConstants.REQUEST_BOOK_SAVE,
              success: ActionConstants.BOOK_SAVE_SUCCESS,
              failure: ActionConstants.BOOK_SAVE_ERROR
            }, data);
          }
        } catch(e) {}
      });

    AppDispatcher.dispatch({
      type: ActionConstants.REQUEST_BOOK_SAVE
    });
  },

  updateBook(modelClass, book, newData) {
    newData.runCloudCode = true;
    let promise = adapter.update(modelClass, book.objectId, newData);
    newData.objectId = book.objectId;

    AppDispatcher.dispatchAsync(promise, {
      request: ActionConstants.REQUEST_BOOK_UPDATE,
      success: ActionConstants.BOOK_UPDATE_SUCCESS,
      failure: ActionConstants.BOOK_UPDATE_ERROR
    }, newData);
  },

  deleteBook(modelClass, book) {
    book.runCloudCode = true;
    let promise = adapter.destroy(modelClass, book.objectId);

    AppDispatcher.dispatchAsync(promise, {
      request: ActionConstants.REQUEST_BOOK_DESTROY,
      success: ActionConstants.BOOK_DESTROY_SUCCESS,
      failure: ActionConstants.BOOK_DESTROY_ERROR
    }, book);
  },

  clearNotifications() {
    AppDispatcher.dispatch({
      type: ActionConstants.CLEAR_NOTIFICATIONS
    });
  }
};