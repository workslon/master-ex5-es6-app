import { Dispatcher } from 'flux';
let dispatcher = new Dispatcher();

export default {
  register(callback) {
    return dispatcher.register(callback);
  },

  dispatch: function (action = {}) {
    console.log(action);
    dispatcher.dispatch(action);
  },

  dispatchAsync: function (promise, types, data = {}) {
    const request = types.request;
    const success = types.success;
    const failure = types.failure;
    const dispatch = this.dispatch;

    dispatch({
      type: request,
      data: data
    });

    promise.then(
      result => {
        dispatch({
          type: success,
          data: data,
          result: result
        });
      },
      error => {
        try {
          error = JSON.parse(error.responseText).error;
        } catch(e) {
          error = error.statusText;
        }

        dispatch({
          type: failure,
          data: data,
          error: error
        });
      }
    );
  }
};