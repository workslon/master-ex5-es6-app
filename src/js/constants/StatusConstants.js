import eNUMERATION from 'eNUMERATION';

const StatusConstants = new eNUMERATION('StatusConstants', [
  'idle',
  'pending',
  'success',
  'error'
]);

export default StatusConstants;